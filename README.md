## Projeto IaC com Terraform

Este projeto contempla a criação de uma infraestrutura básica em cloud AWS, seguindo o conceito de IAC através do Terraform, para consumo de recursos de computação(EC2 Compute).

Os recursos que serão criados são:

**VPC**
- Internet Gateway
- Subnets
- Route tables
- Default security group

**Security Group**
- Security Group dedicado para o projeto de EC2 compute.
- Criação de regras no Security Group.

**EC2 Compute**
- Criação de instância EC2 compute nos recursos previamente provisionados.

As variáveis que definem os valores que serão aplicados no projeto, estão no arquivo **"idwall/variables.tf"**.

**Para execução deste projeto, siga os seguintes passos:**

    $ git clone https://alvesoliveira@bitbucket.org/alvesoliveira/idwall.git
    $ cd idwall
    $ chmod +x build.sh
    $ ./build.sh iac/ apply

Para destruir o projeto, siga os seguintes passos:

    $ ./build.sh iac/ destroy

Caso seja necessário, fique à vontade para realizar um "pull request" neste projeto.