#!/bin/bash
if [ -z $AWS_ACCESS_KEY_ID:x ] || [ -z $AWS_SECRET_ACCESS_KEY:x ] ;
then
  echo "Valor de ACCESS KEY ou de SECRET ACCESS KEY nao foi definido."
  echo "Por favor, insira sua ACCESS KEY:"
  read -s AWS_ACCESS_KEY_ID
  echo "Por favor, insira sua SECRET KEY:"
  read -s AWS_SECRET_ACCESS_KEY
fi
cd $1
terraform $2 -var aws_access_key=$AWS_ACCESS_KEY_ID aws_secret_key=$AWS_SECRET_ACCESS_KEY
