#!/bin/bash
bash 2>&1 <<"USERDATA" | while read line; do echo "$(date --iso-8601=ns) $line"; done | tee -a /var/log/userdata.log
set -xe

# Install docker
sudo yum install docker -y

# Enable docker service
sudo ln -s /etc/init.d/docker /etc/rc3.d/S100docker

# Start service
sudo /etc/init.d/docker start

# Run apache container
sudo docker run --rm --name apache -d -p 80:80 httpd:latest

USERDATA