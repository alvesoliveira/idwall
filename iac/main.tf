provider "aws" {}

module "vpc" {
  source      = "../modules/vpc"

  name        = "${var.name}"
  region      = "${var.region}"
  cidr        = "${var.cidr}"
  subnets     = "${var.subnets}"
  azs         = "${var.azs}"
}

module "sg" {
  source      = "../modules/sg"

  sg_name     = "${var.sg_name}"
  vpc_id      = "${module.vpc.2}"
}

module "sg_rule_ssh" {
  source      = "../modules/sg_rule"

  dport       = "22"
  src         = ["200.100.103.1/32"]
  sg_id       = "${module.sg.1}"
}

module "sg_rule_http" {
  source      = "../modules/sg_rule"

  dport       = "80"
  src         = ["0.0.0.0/0"]
  sg_id       = "${module.sg.1}"
}

module "sg_rule_https" {
  source      = "../modules/sg_rule"

  dport       = "443"
  src         = ["0.0.0.0/0"]
  sg_id       = "${module.sg.1}"
}

module "ec2" {
  source        = "../modules/ec2"

  inst_count    = "${var.inst_count}"
  subnet_id     = "${module.vpc.4}"
  type          = "${var.type}"
  ami           = "${var.ami}"
  key           = "${var.key}"
  sg_id         = "${module.sg.1}"
  size_so       = "${var.size_so}"
  type_disk_so  = "${var.type_disk_so}"
  tag           = "${var.ec2_tag}"
}