variable "name" {
  default = "docker"
}

variable "region" {
  default = "us-east-2"
}

variable "cidr" {
  default = "10.0.0.0/16"
}

variable "subnets" {
  type = "list"
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "azs" {
  type = "list"
  default = ["us-east-2a", "us-east-2b" , "us-east-2c"]
}

// Variáveis do módulo de Security Group
variable "sg_name" {
  default = "docker-sg"
}

// Variáveis do módulo de Security Group Rule
variable "dport" {
  default = "22"
}

variable "dport_http" {
  default = "80"
}

variable "src" {
  type = "list"
  default = ["200.100.103.1/32"]
}

// Variáveis do módulo de EC2
variable "inst_count" {
  default = "1"
}

variable "type" {
  default = "t2.micro"
}

variable "ami" {
  default = "ami-0b59bfac6be064b78"
}

variable "key" {
  default = "hml"
}

variable "size_so" {
  default = 50
}

variable "type_disk_so" {
  default = "gp2"
}

variable "ec2_tag" {
  default = "docker"
}