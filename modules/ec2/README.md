
  
    
      
**MÓDULO PARA CRIAÇÃO DE VPC**        
 Este projeto contém os arquivos necessários para criação de EC2 Compute, na AWS, através da utilização de módulos no Terraform.        
        
Isto quer dizer que, você poderá reutilizar o módulo de criação de "EC2 Compute" em seus projetos do Terraform.        
        
      
***Exemplo de utilização no main.tf:***      
      
        
    module "ec2" {  
      source = "../modules/ec2"  
      
      inst_count = "3"  
      subnet_id = "subnet-c06be5a9"  
      type = "t2.micro"  
      ami = "ami-0b59bfac6be064b78"  
      key = "foo"  
      sg_id = "sg-0e02ae219da4c62d5"  
      size_so = "50"  
      type_disk_so = "gp2"  
      tag = "foo"  
    }

  
  Parâmetros:    

 - **source**:  Define o "PATH" para o módulo em utilização.
 - **inst_count**:  Define o número de instâncias que serão provisionadas.
 - **type**: Define o tipo de instância que será provisionado.
 - **ami**: Define o ID da AMI que será utilizada no provisionamento.
 - **key**: Define o nome da "key-pair" que será utilizada nas instâncias.
 - **sg_id**: Define o ID do Security Group que será utilizado nas instâncias.
 - **size_so**:  Define o tamanho do disco padrão iniciado nas instâncias.
 - **type_disk_so**:  Define o tipo do disco que será utilizado nas instâncias.
 - **tag**:  Define o nome do recurso que será criado.