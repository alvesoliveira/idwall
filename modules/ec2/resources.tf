resource "aws_instance" "module" {
  count = "${var.inst_count}"
  subnet_id = "${element(var.subnet_id, count.index)}"
  instance_type = "${var.type}"
  ami = "${var.ami}"
  key_name = "${var.key}"
  vpc_security_group_ids = [
    "${var.sg_id}"]
  associate_public_ip_address = true
  user_data     = "${file("./files/user_data.sh")}"

  root_block_device {
    volume_size = "${var.size_so}"
    volume_type = "${var.type_disk_so}"
  }

  tags {
    Name = "${var.tag}"
  }
}