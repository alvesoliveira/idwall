
  
**MÓDULO PARA CRIAÇÃO DE SECURITY GROUP**    
 Este projeto contém os arquivos necessários para criação de Security Groups na AWS, através da utilização de módulos no Terraform.    
    
Isto quer dizer que, você poderá reutilizar o módulo de criação de "security groups" em seus projetos do Terraform.    
    
  
***Exemplo de utilização no main.tf:***   
  
    
    module "sg" {  
      source = "../modules/sg"  
      
      sg_name = "foo"  
      vpc_id = "vpc-40308929"  
    }


Parâmetros:

 - **source**:  Define o "PATH" para o módulo que será utilizado.
 - **sg_name**: Define o nome do recurso que será criado.
 - **vpc_id**: Define em que VPC o recurso será criado.