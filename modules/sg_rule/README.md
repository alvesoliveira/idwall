
  
**MÓDULO PARA CRIAÇÃO DE REGRAS DE SECURITY GROUP**    
 Este projeto contém os arquivos necessários para criação de "rules" em Security Groups na AWS, através da utilização de módulos no Terraform.    
    
Isto quer dizer que, você poderá reutilizar o módulo de criação de "rules" em seus projetos do Terraform.    
    
  
***Exemplo de utilização no main.tf:***   
  
    
     module "sg_rule_ssh" {
	      source      = "../modules/sg_rule"        

          dport       = "22"    
          src         = ["200.100.103.1/32"]    
          sg_id       = "sg-0e02ae219da4c62d5"    
        }


Parâmetros:

 - **source**:  Define o "PATH" para o módulo que será utilizado.
 - **dport**: Define a porta de destino que será utilizada na criação da regra.
 - **src**: Define o endereço de origem para a regra que será criada.
 - **sg_id**: Define o "ID" do "Security Group" que será utilizado na criação da regra.

