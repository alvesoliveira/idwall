resource "aws_security_group_rule" "module" {
  type            = "ingress"
  from_port       = "${var.dport}"
  to_port         = "${var.dport}"
  protocol        = "tcp"
  cidr_blocks     = "${var.src}"

  security_group_id = "${var.sg_id}"
}