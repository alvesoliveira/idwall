
  
    
**MÓDULO PARA CRIAÇÃO DE VPC**      
 Este projeto contém os arquivos necessários para criação de VPC's na AWS, através da utilização de módulos no Terraform.      
      
Isto quer dizer que, você poderá reutilizar o módulo de criação de "VPC" em seus projetos do Terraform.      
      
    
***Exemplo de utilização no main.tf:***    
    
      
    module "vpc" {  
      source = "../modules/vpc"  
      
      name = "foo"  
      region = "us-east-1a"  
      cidr = "10.0.0.0/16"  
      subnets = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
      azs = ["us-east-1a", "us-east-1b", "us-east-1b"]  
    }

  
  
Parâmetros:  
  
 - **name**:  Define o nome do recurso que será criado.
 - **region**: Define a região na qual o recurso será criado.
 - **cidr**: Define o blobo CIDR que será utilizado na criação da VPC.
 - **subnets**: Define as "sub-redes" que serão criadas na VPC.
 - **azs**: Define as "zonas de disponiblidade" que as "sub-redes" serão criadas.