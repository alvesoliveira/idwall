variable "name" {
  default = "docker"
}

variable "region" {
  default = "us-east-2"
}

variable "cidr" {
  default = "10.0.0.0/21"
}

variable "subnets" {
  type = "list"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "azs" {
  type = "list"
  default     = ["us-east-2a", "us-east-2b" , "us-east-2c"]
}

variable "enable_dns_hostnames" {
  default     = true
}

variable "enable_dns_support" {
  default     = true
}

variable "enable_nat_gateway" {
  default     = true
}

variable "map_public_ip_on_launch" {
  default     = true
}

variable "private_propagating_vgws" {
  type = "list"
  default     = []
}

variable "public_propagating_vgws" {
  type = "list"
  default     = []
}

variable "tags" {
  default     = {}
}